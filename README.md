# rb_2d_lbm_mrt_for_piml__sample_data

# Author
Dragos B. Chirila (dchirila _(at)_ gmail _(dot)_ com)

# About this repository

Sample data, obtained by:
1. executing the code in the repository:
   [rb_2d_lbm_mrt_for_piml](https://gitlab.com/dchirila/rb_2d_lbm_mrt_for_piml),
   and
2. extracting the last 2 percent of the time-slices, using `cdo`:
```console
cdo seltimestep,23157/23630 \
    result.nc \
    result_rb_2d__Ra_2.5e8__Pr_0.71__maxMach_0.1__t_D_max_diffusive_scaling__0.4__last_2_percent.nc
```

__NOTE:__ See the [code](https://gitlab.com/dchirila/rb_2d_lbm_mrt_for_piml) repository for more details.
